#TODO: change out some lines for applying settings on restart and showing a label saying require restart

extends Node

const CONFIG_DIR: String = "data/saves/"
const CONFIG_FILE_NAME: String = "settings"
const CONFIG_EXTENSION: String = ".tres"
const BASE_DIR: String = "user://"
const CONFIG_PATH: String = BASE_DIR + CONFIG_DIR + CONFIG_FILE_NAME + CONFIG_EXTENSION

#func _process(delta: float) -> void:
	#print(Engine.get_frames_per_second(), Engine.max_fps)
#	Save file information
var first_time_setup: bool = true
var accessibility: Dictionary = {
	"current_locale": "en",
	"small_text_font_size": 20,
	"big_text_font_size": 64
}
var gameplay_options: Dictionary = {
	"max_fps": 60,
	"pause_on_lost_focus": true
}
var video: Dictionary = {
	"borderless": false,
	"fullscreen": false,
	"resolution": Vector2i(1080, 720)
}
var audio: Dictionary = {
	"Master": 100,
	"Music": 100,
	"SFX": 100
}


#region Settings variables


#endregion

#region Saving and loading

func get_or_create_dir(path: String) -> DirAccess:
	var dir := DirAccess.open(BASE_DIR)
	if !dir.dir_exists(path):
		dir.make_dir_recursive(path)
	return dir

func save_settings() -> void:
	var new_save := GameSettingsSave.new()
	new_save.first_time_setup = first_time_setup
	new_save.accessibility = accessibility.duplicate(true)
	new_save.gameplay_options = gameplay_options.duplicate(true)
	new_save.video = video.duplicate(true)
	new_save.audio = audio.duplicate(true)
	
	get_or_create_dir(CONFIG_DIR)
	var save_result := ResourceSaver.save(new_save, CONFIG_PATH)
	
	if save_result != OK:
		push_error("Failed to save settings to: %s" % CONFIG_PATH)
	else:
		print("Settings successfully saved to: %s" % CONFIG_PATH)

func load_settings() -> bool:
	if !ResourceLoader.exists(CONFIG_PATH):
		print("Settings save file not found.")
		return false
	
	print("Settings file was found.")
	var new_load: GameSettingsSave = ResourceLoader.load(CONFIG_PATH, "Resource", ResourceLoader.CACHE_MODE_REUSE)
	
	if new_load == null:
		push_error("Failed to load settings from: %s" % CONFIG_PATH)
		return false
	
	first_time_setup = new_load.first_time_setup
	accessibility = new_load.accessibility.duplicate(true)
	gameplay_options = new_load.gameplay_options.duplicate(true)
	video = new_load.video.duplicate(true)
	audio = new_load.audio.duplicate(true)
	
	return true



#endregion
